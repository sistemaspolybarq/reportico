# Crear un nuevo puerto Reportico
1. Ingresar al vmware a través de la dirección: 172.19.40.10 con usuario administrator@vsphere.local y contraseña polyB2012.
2. Ingresar a la maquina virtual de reportico (Server Contenedores Reportico).
3. Una vez en el servidor la clave de usuario del SO es 040418
4. Copiar la carpeta del reportico que se desea duplicar ubicadas en el escritorio (Producción:8010, Prueba:8011, Historico:8012, Contabilidad:8013).

![](ImagenesReportico/Captura1.PNG)

5. Dentro de la carpeta del reportico que se duplico editar el archivo docker compose, cambiar el servicio, sustituir el nuevo puerto que se le desea colocar y host de base de datos.

![](ImagenesReportico/Captura2.PNG)

6. Abrir la terminal en la carpeta del proyecto y correr el comando **sudo docker-compose up –d** para levantar el contenedor.

![](ImagenesReportico/Captura3.PNG)

7. Correr el comando sudo docker ps para verificar que se esta corriendo el nuevo host de reportico.

![](ImagenesReportico/Captura4.PNG)

---

# Ingresar a un contenedor por medio de la consola.
Para ingresar al contenedor por medio de la consola del SO realizar las siguientes instrucciones:
1. Listar los contenedores con el comando **sudo docker ps** para visualizar los detalles de cada uno y copiar el nombre al que se desea acceder.

![](ImagenesReportico/Captura5.PNG)

2. Ingresar el comando **sudo docker exec -it "nombre del contenedor" /bin/bash** para accerder al contenedor.

