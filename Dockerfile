# https://hub.docker.com/_/php

FROM php:7.3.3-apache

RUN apt-get update 

# dependencias necesarias para el registro de claves publicas de debian

RUN apt-get install -y gnupg debian-keyring debian-archive-keyring

# dependecias y paquetes necesarios para composer y reportico

RUN apt-get install -y zip unzip libpng-dev zlib1g-dev libbz2-dev curl

# solucion para instalar apt-transport-https

RUN sed -i '2,2s/buster/buster*/' /etc/apt/preferences.d/argon2-buster
RUN apt-get install -y apt-transport-https  

# Instalacion de los drivers sql server
# https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15#debian

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -

RUN curl https://packages.microsoft.com/config/debian/9/prod.list > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get -y update \
    && apt-get update


# Si se esta usando la consola web de ocp instalar las siguientes dependencias dentro del contenedor

#RUN ACCEPT_EULA=Y
#RUN apt-get install -y msodbcsql17 mssql-tools unixodbc-dev

# Habilitar extensiones necesarias para reportico y sql server
# Como especifica en la seccion de How to install more PHP extensions en el siguente link:
# https://hub.docker.com/_/php

#RUN docker-php-ext-install pdo gd bz2 \
#    && pecl install sqlsrv pdo_sqlsrv \
#    && docker-php-ext-enable sqlsrv pdo_sqlsrv  

# Instalacion de Composer 
# https://getcomposer.org/download/
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=1.9.0

# Instalacion de Reportico
#RUN composer create-project reportico-web/reportico:6.0.11
#RUN chmod -R 777 .

# Copiamos archivo de configuraciones de reportico a reportico en la imagen para 
#COPY ./config.php reportico/projects/admin/
